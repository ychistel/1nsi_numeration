Exercices
=========

.. exercice::

    Donner l'écriture décimale des nombres entiers positifs codés en binaire.

    a. :math:`101_{2}`
    b. :math:`10111_{2}`
    c. :math:`11001101_{2}`

.. exercice::

    Donner l'écriture binaire des nombres entiers positifs écrits en décimal.

    a. :math:`54`
    b. :math:`132`
    c. :math:`245`

.. exercice::

    Convertir en notation binaire les nombres hexadécimaux suivants:

    a. :math:`16_{16}`
    b. :math:`3B_{16}`
    c. :math:`5E1_{16}`

.. exercice::

    Convertir en notation hexadécimale les nombres binaires suivants:

    a. :math:`10101_{2}`
    b. :math:`100100_{2}`
    c. :math:`111100001111_{2}`

.. exercice::

    Convertir en notation décimale les nombres suivants écrits en notation hexadécimale:

    a. :math:`3E_{16}`
    b. :math:`4F8_{16}`
    c. :math:`A53C_{16}`

.. exercice::

    Convertir en écriture hexadécimale les nombres suivants:

    a. :math:`168_{10}`
    b. :math:`1001_{10}`
    c. :math:`1616_{10}`